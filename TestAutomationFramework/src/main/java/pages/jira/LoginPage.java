package pages.jira;

import com.telerikacademy.testframework.Utils;
import org.openqa.selenium.WebDriver;

public class LoginPage extends BaseJiraPage{

    public LoginPage(WebDriver driver) {
        super(driver, "jira.url");
    }

    public void loginUser(String userKey) {
        String username = Utils.getConfigPropertyByKey("jira.users." + userKey + ".username");
        String password = Utils.getConfigPropertyByKey("jira.users." + userKey + ".password");

        navigateToPage();

        actions.waitForElementVisible("jira.loginPage.username");
        actions.typeValueInField(username, "jira.loginPage.username");
        actions.clickElement("jira.loginPage.buttonContinueLogin");

        actions.waitForElementClickable("jira.loginPage.buttonContinueLogin");

        actions.waitForElementVisible("jira.loginPage.password");
        actions.typeValueInField(password, "jira.loginPage.password");
        actions.clickElement("jira.loginPage.buttonContinueLogin");

        actions.waitForElementVisible("jira.loginPage.avatar");
    }
}
