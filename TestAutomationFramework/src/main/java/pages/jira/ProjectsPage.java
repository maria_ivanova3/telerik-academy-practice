package pages.jira;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;

public class ProjectsPage extends BaseJiraPage {

    private static final String TEXT_SUMMARY_STORY = "Check information in the About us page";
    private static final String TEXT_DESCRIPTION_STORY = "Description:\n" +
            "The About us page is full when site redirects to it.\n" +
            "Steps to reproduce:\n" +
            "Load the tested site\n" +
            "Scroll at the end of the site\n" +
            "Click “About us“ page\n" +
            "Expected results:\n" +
            "Directed to page with the information about the company and site.\n" +
            "Actual results:\n" +
            "Directed to page with the information about the company and site.";
    private static final String TEXT_SUMMARY_BUG = "Empty field in the About us page";
    private static final String TEXT_DESCRIPTION_BUG = "Description:\n" +
            "The About us page is empty when site redirects to it.\n" +
            "Steps to reproduce:\n" +
            "Load the tested site\n" +
            "Scroll at the end of the site\n" +
            "Click “About us“ page\n" +
            "Expected results:\n" +
            "Directed to page with the information about the company and site.\n" +
            "Actual results:\n" +
            "Directed to an empty page.\n" +
            "Severity level: Moderate";

    private static final String LINK_ISSUE = "WEB-19";

    public ProjectsPage(WebDriver driver) {
        super(driver, "jira.urlProjectsPage");}


        public void createStory(){
            actions.waitForElementClickable("jira.projectsPage.clickOnProjectsIssues");
            actions.clickElement("jira.projectsPage.clickOnProjectsIssues");

            actions.waitForElementClickable("jira.projectsPage.createButton");
            actions.clickElement("jira.projectsPage.createButton");

            actions.clickElement("jira.projectsPage.chooseProject");
            actions.clickElement("jira.projectsPage.titleProject");
            actions.clickElement("jira.projectsPage.chooseIssue");
            actions.clickElement("jira.projectsPage.typeIssueStory");

            actions.waitForElementClickable("jira.projectsPage.summary");

            actions.typeValueInField(TEXT_SUMMARY_STORY,"jira.projectsPage.summary");
            actions.typeValueInField(TEXT_DESCRIPTION_STORY, "jira.projectsPage.description");

            actions.clickElement("jira.projectsPage.menuPriority");
            actions.clickElement("jira.projectsPage.setPriority");
            actions.clickElement("jira.projectsPage.setAssignee");

            actions.waitForElementClickable("jira.projectsPage.createIssueButton");

            actions.clickElement("jira.projectsPage.createIssueButton");


        }


        public void createBug() {
            actions.waitForElementClickable("jira.projectsPage.createButton");
            actions.clickElement("jira.projectsPage.createButton");

            actions.clickElement("jira.projectsPage.chooseProject");
            actions.clickElement("jira.projectsPage.titleProject");
            actions.clickElement("jira.projectsPage.chooseIssue");
            actions.clickElement("jira.projectsPage.typeIssueBug");

            actions.waitForElementClickable("jira.projectsPage.summary");

            actions.typeValueInField(TEXT_SUMMARY_BUG,"jira.projectsPage.summary");
            actions.typeValueInField(TEXT_DESCRIPTION_BUG, "jira.projectsPage.description");

            actions.clickElement("jira.projectsPage.menuPriority");
            actions.clickElement("jira.projectsPage.setPriority");
            actions.clickElement("jira.projectsPage.setAssignee");

            actions.waitForElementClickable("jira.projectsPage.createIssueButton");

            actions.clickElement("jira.projectsPage.createIssueButton");

        }

        public void link_Bug_to_Story(){
            actions.waitForElementClickable("jira.projectsPage.clickOnProjectsIssues");
            actions.clickElement("jira.projectsPage.clickOnProjectsIssues");

            actions.waitForElementClickable("jira.projectsPage.clickFirstIssue");
            actions.clickElement("jira.projectsPage.clickFirstIssue");

            actions.waitForElementClickable("jira.projectsPage.clickLinkIssueButton");
            actions.clickElement("jira.projectsPage.clickLinkIssueButton");

            actions.waitForElementClickable("jira.projectsPage.clickBlocksDropDown");
            actions.clickElement("jira.projectsPage.clickBlocksDropDown");
            actions.clickElement("jira.projectsPage.setBlocksDropDown");
            actions.clickElement("jira.projectsPage.clickChooseIssueLink");
            Actions actions = new Actions(driver);
            actions.sendKeys("WEB-19");
            actions.sendKeys(Keys.ENTER);

//            actions.clickElement("jira.projectsPage.setIssueLink");
//            actions.clickElement("jira.projectsPage.clickLinkButton");

        }

}
