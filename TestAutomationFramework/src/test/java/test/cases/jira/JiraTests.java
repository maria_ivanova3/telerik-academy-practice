package test.cases.jira;


import org.junit.Test;
import pages.jira.ProjectsPage;



public class JiraTests extends BaseTest {

    @Test
    public void createStoryAndBug_When_CreateButtonClicked() {
        ProjectsPage projectsPageStory = new ProjectsPage(actions.getDriver());
        projectsPageStory.createStory();

        ProjectsPage projectsPageBug = new ProjectsPage(actions.getDriver());
        projectsPageBug.createBug();

    }


    @Test
    public void link_Bug_to_Story(){
        ProjectsPage linkBugToStory = new ProjectsPage(actions.getDriver());
        linkBugToStory.link_Bug_to_Story();

    }

}



