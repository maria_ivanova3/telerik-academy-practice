package test.cases.jira;

import com.telerikacademy.testframework.UserActions;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import pages.jira.LoginPage;

public class BaseTest {

    static UserActions actions = new UserActions();

    @BeforeClass
    public static void setUp() {
        UserActions.loadBrowser("jira.url");
    }

    @BeforeClass
    public static void login() {

        LoginPage loginPage = new LoginPage(actions.getDriver());
        loginPage.loginUser("user");
        loginPage.assertPageNavigated();
    }

    @AfterClass
    public static void tearDown() {
        UserActions.quitDriver();
    }





}

