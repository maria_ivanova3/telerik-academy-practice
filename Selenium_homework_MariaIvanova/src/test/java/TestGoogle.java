import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class TestGoogle {

    @Test
    public void navigateToGoogle(){
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\mi6o9\\IdeaProjects\\QA-Module-3\\chromedriver.exe");
        WebDriver webDriver = new ChromeDriver();

        webDriver.get("https://www.google.com/");
        Assert.assertTrue(true);
        webDriver.findElement(By.id("L2AGLb")).click();
        WebElement search_google = webDriver.findElement(By.name("q"));
        search_google.sendKeys("Telerik Academy Alpha");
        search_google.sendKeys(Keys.RETURN);
        webDriver.findElement(By.partialLinkText("IT Career Start in 6 Months - Telerik Academy Alpha")).click();

        String exp_title = "IT Career Start in 6 Months - Telerik Academy Alpha";
        String act_title = webDriver.getTitle();

        Assert.assertEquals(exp_title, act_title);

    }


}
