import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class TestBing {

    @Test
    public void navigateToBing(){
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\mi6o9\\IdeaProjects\\QA-Module-3\\chromedriver.exe");
        WebDriver webDriver = new ChromeDriver();

        webDriver.get("https://www.bing.com/");
        Assert.assertTrue(true);
        webDriver.findElement(By.id("sb_form_q")).sendKeys("Telerik Academy Alpha");
        webDriver.findElement(By.id("search_icon")).click();
        webDriver.findElement(By.partialLinkText("IT Career Start in 6 Months - Telerik Academy Alpha")).click();

        String exp_title = "IT Career Start in 6 Months - Telerik Academy Alpha";
        String act_title = webDriver.getTitle();

        Assert.assertEquals(exp_title, act_title);

    }


}
