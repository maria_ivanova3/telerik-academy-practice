package com.telerikacademy.testframework.pages;

import org.openqa.selenium.WebDriver;

import static com.telerikacademy.testframework.pages.RegistrationPage.generateWord;

public class PostPage extends BasePage {

    public String textBefore = generateWord(10);
    public String textAfter = generateWord(20);

    public PostPage(WebDriver driver) {super(driver, "home.page");}

    public void createPublicPost() {
        actions.clickElement("postpage.addNewPostButton");
        actions.clickElement("postpage.selectVisibilityMenu");
        actions.clickElement("postpage.publicVisibility");
        actions.typeValueInField(textBefore, "postpage.textField");
        actions.clickElement("postpage.savePostButton");
    }

    public void likePublicPost() {
        actions.waitForElementClickable("postpage.latestPostsButton");
        actions.clickElement("postpage.latestPostsButton");
        actions.waitForElementClickable("postpage.likePostButton");
        actions.clickElement("postpage.likePostButton");
    }

    public void dislikePublicPost() {
        actions.waitForElementClickable("postpage.latestPostsButton");
        actions.clickElement("postpage.latestPostsButton");
        actions.waitForElementClickable("postpage.dislikePostButton");
        actions.clickElement("postpage.dislikePostButton");
    }

    public void editPublicPost() {
        navigateToPost();
        actions.waitForElementClickable("postpage.editPostButton");
        actions.clickElement("postpage.editPostButton");
        actions.clickElement("postpage.selectVisibilityMenu");
        actions.clickElement("postpage.publicVisibility");
        actions.typeValueInField(textAfter, "postpage.textField");
        actions.clickElement("postpage.savePostButton");
    }

    public void deletePublicPost() {
        navigateToPost();
        actions.waitForElementClickable("postpage.deletePostButton");
        actions.clickElement("postpage.deletePostButton");
        actions.waitForElementClickable("postpage.dropDownDeleteMenu");
        actions.clickElement("postpage.dropDownDeleteMenu");
        actions.clickElement("postpage.deletePostChoice");
        actions.clickElement("postpage.deleteSubmitButton");
    }

    public void navigateToPost() {
        actions.waitForElementClickable("postpage.latestPostsButton");
        actions.clickElement("postpage.latestPostsButton");
        actions.waitForElementClickable("postpage.explorePostButton");
        actions.clickElement("postpage.explorePostButton");
    }

}
