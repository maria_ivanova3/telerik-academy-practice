package com.telerikacademy.testframework.pages;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import static com.telerikacademy.testframework.pages.RegistrationPage.generateWord;

public class CommentPage extends BasePage {

    public static final String MESSAGE = "Comment message don't match";
    public String commentMessage;
    public String commentEditedMessage;

    public CommentPage(WebDriver driver) {
        super(driver, "home.page");
    }

    public void navigateToPost() {
        actions.waitForElementClickable("commentPage.latestPostsButton");
        actions.clickElement("commentPage.latestPostsButton");
        actions.waitForElementClickable("commentPage.browseAllPublicPostsButton");
        actions.clickElement("commentPage.browseAllPublicPostsButton");
        actions.waitForElementClickable("commentPage.exploreThisPostButton");
        actions.clickElement("commentPage.exploreThisPostButton");
    }

    public void navigateToComment() {
        actions.waitForElementClickable("commentPage.showCommentsButton");
        actions.clickElement("commentPage.showCommentsButton");
    }

    public void createComment() {
        actions.waitForElementPresent("commentPage.messageField");
        commentMessage = generateWord(20);
        actions.typeValueInField(commentMessage, "commentPage.messageField");
        actions.clickElement("commentPage.postCommentButton");
    }

    public void likeComment() {
        actions.waitForElementClickable("commentPage.likeCommentButton");
        actions.clickElement("commentPage.likeCommentButton");
    }

    public void dislikeComment() {
        actions.waitForElementClickable("commentPage.dislikeCommentButton");
        actions.clickElement("commentPage.dislikeCommentButton");
    }

    public void editComment() {
        actions.waitForElementClickable("commentPage.editCommentButton");
        actions.clickElement("commentPage.editCommentButton");
        actions.waitForElementPresent("commentPage.messageField");
        commentEditedMessage = generateWord(30);
        actions.typeValueInField(commentEditedMessage, "commentPage.messageField");
        actions.clickElement("commentPage.submitEditCommentButton");
    }

    public void deleteComment() {
        actions.waitForElementClickable("commentPage.deleteCommentButton");
        actions.clickElement("commentPage.deleteCommentButton");
        actions.waitForElementPresent("commentPage.deleteOptionsList");
        actions.selectFromDropDownMenu("true", "commentPage.deleteOptionsList");
        actions.clickElement("commentPage.submitDeleteCommentButton");
    }

    public void asserCommentMessageMatches() {
        navigateToPost();
        navigateToComment();
        actions.waitForElementVisible("commentPage.commentMessage");
        String actualCommentMessage = actions.getTextOfElement("commentPage.commentMessage");
        Assert.assertEquals(MESSAGE, commentMessage, actualCommentMessage);
    }

    public void assertDislikeButtonPresent() {
        actions.waitForElementPresent("commentPage.dislikeCommentButton");
        actions.assertElementPresent("commentPage.dislikeCommentButton");
    }

    public void assertLikeButtonPresent() {
        actions.waitForElementPresent("commentPage.likeCommentButton");
        actions.assertElementPresent("commentPage.likeCommentButton");
    }

    public void assertCommentMessageEdited() {
        navigateToPost();
        navigateToComment();
        actions.waitForElementVisible("commentPage.commentMessage");
        String actualCommentEditedMessage = actions.getTextOfElement("commentPage.commentMessage");
        Assert.assertEquals(MESSAGE, commentEditedMessage, actualCommentEditedMessage);
    }

    public void assertDeleteCommentMessagePresent() {
        actions.assertElementPresent("commentPage.deletedCommentMessage");
    }
}
