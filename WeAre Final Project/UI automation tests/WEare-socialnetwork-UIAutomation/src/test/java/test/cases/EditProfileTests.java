package test.cases;

import com.telerikacademy.testframework.pages.EditPersonalProfilePage;
import com.telerikacademy.testframework.pages.LoginPage;
import org.junit.After;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)

public class EditProfileTests extends BaseTestSetup{

    EditPersonalProfilePage editUserProfile = new EditPersonalProfilePage(actions.getDriver());

    LoginPage loginPage =new LoginPage(actions.getDriver());

    @Before
    public void loginUser() {
        loginPage.loadLoginPage();
        loginPage.loginUserValidCredentials("data.username","data.userPassword");
        loginPage.assertLogoutButtonPresent();
        actions.assertElementPresent("landingpage.personalProfileButton");
    }

    @After
    public void logOutUser() {
        loginPage.logoutUser();
        loginPage.assertLogoutMessagePresent();
    }

    @Test
    public void tc001EditUserName() {
        editUserProfile.navigateToEditProfilePage();
        editUserProfile.editName();
        editUserProfile.assertNameMatches();
    }

    @Test
    public void tc002EditBio() {
        editUserProfile.navigateToEditProfilePage();
        editUserProfile.editBio();
        editUserProfile.assertBioMatches();
    }

}
