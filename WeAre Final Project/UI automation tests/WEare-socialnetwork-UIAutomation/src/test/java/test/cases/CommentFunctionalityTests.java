package test.cases;

import com.telerikacademy.testframework.pages.CommentPage;
import com.telerikacademy.testframework.pages.LoginPage;
import org.junit.After;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)

public class CommentFunctionalityTests extends BaseTestSetup {

    CommentPage commentPage = new CommentPage(actions.getDriver());

    LoginPage loginPage =new LoginPage(actions.getDriver());

    @Before
    public void loginUser() {
        loginPage.loadLoginPage();
        loginPage.loginUserValidCredentials("data.username","data.userPassword");
        loginPage.assertLogoutButtonPresent();
        actions.assertElementPresent("landingpage.personalProfileButton");
    }

    @After
    public void logOutUser() {
        loginPage.logoutUser();
        loginPage.assertLogoutMessagePresent();
    }

    @Test
    public void tc001CreateComment() {
        commentPage.navigateToPost();
        commentPage.createComment();
        commentPage.asserCommentMessageMatches();
    }

    @Test
    public void tc002LikeComment() {
        commentPage.navigateToPost();
        commentPage.navigateToComment();
        commentPage.likeComment();
        commentPage.assertDislikeButtonPresent();
    }

    @Test
    public void tc003DislikeComment() {
        commentPage.navigateToPost();
        commentPage.navigateToComment();
        commentPage.dislikeComment();
        commentPage.assertLikeButtonPresent();
    }

    @Test
    public void tc004EditComment() {
        commentPage.navigateToPost();
        commentPage.navigateToComment();
        commentPage.editComment();
        commentPage.assertCommentMessageEdited();
    }

    @Test
    public void tc005DeleteComment() {
        commentPage.navigateToPost();
        commentPage.navigateToComment();
        commentPage.deleteComment();
        commentPage.assertDeleteCommentMessagePresent();
    }


}
